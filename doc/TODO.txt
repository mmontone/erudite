Some TODOs and ideas:

* Implement documentation chunks
* Look here for inspiration: 
   - http://mainesail.umcs.maine.edu/software/LPLisp/
   - http://mainesail.umcs.maine.edu/pubs/Papers/2010/ilc2010.pdf

* Implement commands:
    - @chunk - Define a chunk
    - @ignore - Ignore a piece of text
    - @include - Include files for literate processing
    - @show-chunk - Display a chunk
    - @define - Define a 'definition'. Names a piece of code and removes it to be inserted later by @insert (see LP/Lisp paper)
    - @insert - Inserts a defined piece of text
    - @doc - At the beggining of a comment. To parse as literate doc if implicit comments are disabled.
    - ...

    Look at PL/Lisp list of commands

* Allow nested chunks?

* A PEG parser? (esrap)

* Ensure the library is portable (use cl-ppcre, esrap, clon, etc)

* Command line interface via Clon

* Single function with input/output type selection:

    (erudite *out* *my-files* :syntax :latex)
    (erudite *out* *my-files* :syntax :sphinx)
    (erudite *out* *my-files* :syntax :html)
    (erudite *out* *my-files* :syntax :markdown)

* Custom syntax for multiple output types generation?:
    
    (erudite *out* *my-files* :syntax :erudite :output-type :latex)
    (erudite *out* *my-files* :syntax :erudite :output-type :sphinx)
    (erudite *out* *my-files* :syntax :erudite :output-type :html)
    (erudite *out* *my-files* :syntax :erudite :output-type :markdown)

* Custom syntax look like?:

   - Latex like:

         \include{my-file}
         \chunk[my-chunk]{...}
         \ref{my-function}

   - Tags like (PL/Lisp version 2):
         @include my-file
         @chunk
            ...
         @end chunk
         @itemize
            @item Item 1
            @item Item 2
         @ref my-function

IDEA for this:  make the '@' or '\' character configurable.

* Use some kind of reader syntax?
    - Look at: https://www.cs.utah.edu/plt/publications/icfp09-fbf.pdf

* Assume #| |# and ;;; (three colons) comments (like in pbook.el) contain literate text. Maybe let the user configure that too? (for ;;, etc)

* Allow different options depending on the type of input and output. So, this should be the signature:

       (erudite out files &rest options)

  with options being :syntax, :output-type, plus specific options for input type and output-type.

  Example:

       (erudite out files :syntax :latex :document-class :book
                                             :listings t
                                             :date "\\today"
                                             :author "Mariano Montone"
                                             :margins "0.5")

  Options that don't apply to a particular input or output are ignored (maybe with a warning?)

* Emacs function to process and view the resulting pdf? (like in pbook.el)

  For example, this is a slightly modified version of what's in pbook.el:

(defun pbook-buffer-view-pdf ()
  "Generate and display PDF from the current buffer.
The intermediate files are created in the standard temporary
directory."
  (interactive)
  (save-window-excursion
    (pbook-buffer))
  (with-current-buffer "*pbook*"
    (let ((texfile (pbook-tmpfile "pbook" "tex"))
          (pdffile (pbook-tmpfile "pbook" "pdf"))
	  (idxfile (pbook-tmpfile "pbook" "idx")))
      (write-region (point-min) (point-max) texfile)
      ;; Possibly there is a better way to ensure that LaTeX generates
      ;; the table of contents correctly than to run it more than
      ;; once, but I don't know one.
      (shell-command (format "\
  cd /tmp; latex %s && \
  pdflatex %s"
                             texfile
			     texfile))
      (find-file pdffile))))



