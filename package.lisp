(defpackage #:erudite
  (:use #:cl #:cl-ppcre)
  (:export #:erudite)
  (:documentation "Erudite is a Literate Programming System for Common Lisp"))

